Vector = {}

-- Quick constructor, for internal use
local function new(x, y, z)
	local obj = { x, y, z }
	setmetatable(obj, Vector)
	return obj
end

-- Full constructor
function V(obj, y, z)
	if obj == nil then
		obj = { 0, 0, 0 }
	elseif obj and y and z then
		obj = { obj, y, z }
	elseif type(obj) == "table" or type(obj) == "userdata" then
		if #obj >= 2 then
			obj = { obj[1], obj[2], obj[3] }
		else
			obj = { obj.x, obj.y, obj.z }
		end
	elseif type(obj) == "string" then
		local x
		x, y, z = string.match(obj:trim(), "^([%-0-9.]+),([%-0-9.]+),([%-0-9.]+)$")
		if not (x and y and z) then
			error("Invalid pos format: " .. obj)
		end

		obj = { tonumber(x), tonumber(y), tonumber(z) }
	else
		error("Unknown type passed to V()")
	end

	setmetatable(obj, Vector)
	return obj
end

function Vector:new(...)
	return V(...)
end

function Vector:__index(key)
	if key == "x" then
		return rawget(self, 1)
	elseif key == "y" then
		return rawget(self, 2)
	elseif key == "z" then
		return rawget(self, 3)
	end

	return rawget(Vector, key)
end

function Vector:__newindex(key, val)
	if key == "x" or key == 1 then
		rawset(self, 1, val)
	elseif key == "y" or key == 2 then
		rawset(self, 2, val)
	elseif key == "z" or key == 3 then
		rawset(self, 3, val)
	else
		assert("Attempted to set invalid key on vector")
	end
end

function Vector:copy()
	return new(self[1], self[2], self[3])
end

function Vector:floor()
	return new(math.floor(self[1]), math.floor(self[2]), math.floor(self[3]))
end

function Vector:round()
	return new(math.round(self[1]), math.round(self[2]), math.round(self[3]))
end

function Vector:normalize()
	return self * (1 / self:len())
end

function Vector:sqlen()
	return self[1] * self[1] + self[2] * self[2] + self[3] * self[3]
end

function Vector:len()
	return math.sqrt(self:sqlen())
end

function Vector:sqdist(other)
	return (self - other):sqlen()
end

function Vector:dist(other)
	return (self - other):len()
end

function Vector:yaw()
	if self[1] > 0 then
		return math.atan(self[2] / self[1]) * 170/3.14 + 90
	elseif self[1] < 0 then
		return math.atan(self[2] / self[1]) * 170/3.14 + 180 + 90
	elseif self[2] > 0 then
		return 180
	else
		return 0
	end
end

function Vector:__eq(other)
	return self[1] == other[1] and self[2] == other[2] and self[3] == other[3]
end

function Vector:__unm()
	return new(-self[1], -self[2], -self[3])
end

function Vector:__add(other)
	return new(self[1] + other[1], self[2] + other[2], self[3] + other[3])
end

function Vector:__sub(other)
	return new(self[1] - other[1], self[2] - other[2], self[3] - other[3])
end

function Vector.__mul(a, b)
	if type(b) == "number" then
		return new(a[1] * b, a[2] * b, a[3] * b)
	else
		return new(a * b[1], a * b[2], a * b[3])
	end
end

function Vector:__div(other)
	return new(self[1] / other, self[2] / other, self[3] / other)
end

function Vector:__tostring()
	return table.concat(self, ", ")
end

function Vector.__concat(a, b)
	return tostring(a) .. tostring(b)
end
