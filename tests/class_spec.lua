dofile("mods/lua_ext/class.lua")

describe("class", function()
	it("no constructor", function()
		local Class = class()

		assert.is_true(isDerivedFrom(Class:new(), Class))
	end)

	it("with constructor", function()
		local MyClass = class()
		function MyClass:constructor(a, b)
			self.a = a
			self.b = b
		end

		function MyClass:add()
			return self.a + self.b
		end

		assert(getmetatable(MyClass))
		assert(MyClass.new)

		local obj = MyClass:new(1, 2)
		assert.is_true(isDerivedFrom(obj, MyClass))
		assert.equals(1, obj.a)
		assert.equals(2, obj.b)
		assert.equals(3, obj:add())
	end)

	it("subclass", function()
		local Base = class()
		function Base:constructor(a)
			self.a = a
		end

		local SubClass = class(Base)
		function SubClass:constructor(a, b)
			Base.constructor(self, a)

			self.b = b
		end

		function SubClass:add()
			return self.a + self.b
		end

		local obj = SubClass:new(1, 2)
		assert.is_true(isDerivedFrom(obj, SubClass))
		assert.is_true(isDerivedFrom(obj, Base))
		assert.equals(1, obj.a)
		assert.equals(2, obj.b)
		assert.equals(3, obj:add())
	end)
end)
