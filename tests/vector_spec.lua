_G.Vector = {}
_G.V = {}
dofile("mods/lua_ext/Vector.lua")

describe("Vector", function()
	describe("new", function()
		it("empty constructor", function()
			local v = Vector:new()
			assert.equal(0, v.x)
			assert.equal(0, v.y)
			assert.equal(0, v.z)
		end)

		it("position table constructor", function()
			local u = Vector:new({ x = 1, y = 2, z = 3 })
			assert.equal(1, u.x)
			assert.equal(2, u.y)
			assert.equal(3, u.z)
		end)

		it("string constructor", function()
			local u = Vector:new("1,2.3,-3")
			assert.equal(1, u.x)
			assert.equal(2.3, u.y)
			assert.equal(-3, u.z)
		end)

		it("copy constructor", function()
			local u = Vector:new({ x = 1, y = 2, z = 3 })
			assert.equal(1, u.x)
			assert.equal(2, u.y)
			assert.equal(3, u.z)

			local v = Vector:new(u)
			u.x = 12
			assert.equal(1, v.x)
			assert.equal(2, v.y)
			assert.equal(3, v.z)
			assert.equal(12, u.x)
			assert.equal(2, u.y)
			assert.equal(3, u.z)
		end)

		it("component constructor", function()
			local u = Vector:new(1, 2, 3)
			assert.equal(1, u.x)
			assert.equal(2, u.y)
			assert.equal(3, u.z)
		end)

		it("alias", function()
			local u = V(1, 2, 3)
			assert.equal(1, u.x)
			assert.equal(2, u.y)
			assert.equal(3, u.z)
		end)
	end)

	it("equals", function()
		local u = Vector:new({ x = 1, y = 2, z = 3 })
		local v = Vector:new({ x = 1, y = 2, z = 3 })
		local t = Vector:new({ x = 3, y = 2, z = 3 })

		assert.is_true(u == v)
		assert.is_false(u == t)
	end)

	it("add", function()
		local u = Vector:new({ x = 1, y = 2, z = 3 })
		local v = Vector:new({ x = 1, y = 2, z = 3 })

		local x = u + v
		assert.is_equal(V(1, 2, 3), u)
		assert.is_equal(V(1, 2, 3), v)
		assert.is_equal(V(2, 4, 6), x)
	end)

	it("mult", function()
		local u = Vector:new({ x = 1, y = 2, z = 3 })

		local x = u * 2
		local y = 2 * u
		assert.is_equal(V(1, 2, 3), u)
		assert.is_equal(V(2, 4, 6), x)
		assert.is_equal(V(2, 4, 6), y)
	end)

	it("div", function()
		local u = Vector:new(2, 4, 6)

		local x = u / 2
		assert.is_equal(V(2, 4, 6), u)
		assert.is_equal(V(1, 2, 3), x)
	end)

	it("stringify", function()
		local u = Vector:new({ x = 1, y = 2, z = 3 })
		assert.is_equal("1, 2, 3", tostring(u))

		local v = Vector:new({ x = 1.2, y = 2.66, z = 3.7 })
		assert.is_equal("1.2, 2.66, 3.7", tostring(v))
		assert.is_equal("pos: 1.2, 2.66, 3.7", "pos: " .. v)
		assert.is_equal("1.2, 2.66, 3.7 away", v .. " away")
	end)
end)
