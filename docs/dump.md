# Dump

* `basic_dump(o)` - returns a string representing o. Tables are not recursed into.
* `dump(o)` - can return a debug string of any object, except userdata or functions. Singleline
* `dump_multiline(o)` - similar to above, but each value in a table is on a new line.
