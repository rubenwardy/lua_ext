# Vector

## Constructors

`V` can be used as shorthand for `Vector:new()`.

* `Vector:new()` - default constructor, `x = y = z = 0`.
* `Vector:new(x, y, z)` - component constructor.
* `Vector:new(table)` - [position table](#position-table) constructor.
* `Vector:new(str)` - string constructor, ie: "1, 2, 3".
* `Vector:new(vec)` - copy constructor.


## Members

A Vector will contain `x`, `y`, and `z`.


## Operators

These operators will not mutate the original vector.

* Equals: `u == v`.
* Unary minus: `-u`.
* Add: `u + v`.
* Subtract: `u - v`.
* Multiply: `u * scalar`.
* Divide: `u / scalar`.
* To string: `tostring(u)`.
* Concat: `"pos " .. u`.

Example:

```lua
local pos = entity:get_pos() + Vector:new(1, 2, 3)
```


## Methods

These methods will not mutate the original vector.

* `copy()` - Copies vector, good for chaining.
* `sqlen()` - gets length squared.
* `len()` - gets length.
* `sqdist(other)` - Square distance to `other`.
* `dist(other)` - Distance to `other`.
* `yaw()` - gets yaw.
* `floor()` - returns floored vector.
* `normalize()` - returns vector in same direction, but with a length of 1.

Here's an example to move a entity in a direction:

```lua
local pos = entity:get_pos()
local delta = pos:normalize() * 1 * dtime
entity:set_pos(pos + delta)
```

## Position Table

A table with the following keys:

* `x` - Float, horizontal left/right.
* `y` - Float, horizontal up/down.
* `z` - Optional Integer, vertical up/down. Only present in 3D position tables.
