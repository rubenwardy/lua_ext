# Lua Improvements

Custom extensions and improvements to Lua's standard library.

## Math

* `math.hypot`
* `math.sign(number, tolerance)`
    * 1 if > `tolerance`
    * -1 if < `-tolerance`
    * 0 otherwise
* `math.round`

## String

* `string[i]` - access individual character.
* `string:trim()` - removes whitespace from beginning and end of string.
* `string:split(separator, include_empty, max_splits, sep_is_pattern)`
    * splits string on 'separator', returns table.
    * Separator defaults to `,` if nil.
    * `include_empty` - true to allow empty strings in returned table.
    * `max_splits` - max number of elements to be returned before stopping.
    * `sep_is_pattern` - true if `separator` is a Lua pattern
* `string:color(color)` - returns a colored string, where color is a valid Color

## Pairs

* Support for `__pairs` and `__ipairs` in Lua 5.1.

## Table

* `table.copy(t)` - deep copy table.
* `table.filter(t, func)` - For numeric tables. Filter table by func.
    * `func` - Takes `value`, returns boolean.
* `table.index_of(t, val)` - find index of `val` in `t`.
* `table.map(t, func)` - Map table using map function.
    * `func` - Takes `value`, returns new value.
