# Set

## Constructors

* `Set:new()`
* `Set:new(iterable)`
* `Set:new(set)` - copy constructor.

## Operators

* `==` - exactly the same values.
* `+` - union.
* `-` - difference.

## Methods

Modify:

* `add(value)`
* `add_all(iterable)`
* `remove(value)`
* `remove_all(iterable)`

Inspect:

* `to_table()` - 1-indexed table.
* `pairs()`

New sets:

* `union(other)` - all values in both sets.
* `difference(other)` - all values in self not in other.
* `intersection(other)` - all values that are in both.

Query:

* `contains(value)` - returns boolean.
* `is_subset` - boolean, other is subset of self.
* `is_disjoint` - boolean, sets have nothing in common.
