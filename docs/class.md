# Classes

* `class(constructor)`
    * Create a class with an optional constructor.
    * `constructor(self, ...)` where `...` is the arguments given to `Class:new()`
    * Example:

      ```lua
      MyClass = class(function(self, a, b)
              self.a = a
              self.b = b
          end)

      local obj = MyClass:new(1, 2)
      ```

* `isDerivedFrom(a, b)` - returns true if `b` is somewhere in `a` metatable chain.
                          For example, `isDerivedFrom(ChatCommand:new(), ChatCommand) == true`.
