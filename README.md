# Lua ext utility library

This is a collection of extensions to the Lua language, including new
utilities and classes.

Awaiting a better name.

* `class()` function for easy OOP.
* Vector class with operator overloads.
* Set class with common operations.
* Math, table, and string methods.
* `dump()`

## License

Licensed under MIT / CC0 / public domain, except for:

- dump.lua: from Minetest, licensed under LGPLv2.1 or later
- string.split: from Minetest, licensed under LGPLv2.1 or later
