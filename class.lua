local Object = {}
Object.__index = Object
function Object:new(...)
	local obj = {}
	setmetatable(obj, self)

	obj:constructor(...)

	return obj
end
function Object:constructor()
end

function class(parent)
	parent = parent or Object
	assert(type(parent) == "table" and parent.new)

	local class_object = {
		constructor = function(self, ...)
			assert(#({...}) == 0, "Default constructor accepts no arguments. Define a constructor.")
			parent.constructor(self)
		end
	}
	class_object.__index = class_object

	setmetatable(class_object, parent)

	return class_object
end

function isDerivedFrom(a, b)
	local class = a
	repeat
		class = getmetatable(class)
	until not class or class == b

	return class == b
end
