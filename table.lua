local function deepcopy(o)
	return type(o) == "table" and table.copy(o) or o
end

function table.copy(t)
	local copy = {}
	for key, value in pairs(t) do
		copy[deepcopy(key)] = deepcopy(value)
	end
	setmetatable(copy, getmetatable(t))

	return copy
end

function table.filter(list, func)
	local ret = {}
	for i, value in ipairs(list) do
		if func(value, i) then
			ret[#ret + 1] = value
		end
	end
	return ret
end

function table.index_of(list, val)
	for i, v in ipairs(list) do
		if v == val then
			return i
		end
	end
	return nil
end

function table.map(list, func)
	local ret = {}
	for key, value in pairs(list) do
		ret[key] = func(value, key)
	end
	return ret
end

if not _ENV then
	-- 5.1

	local old_pairs = pairs
	_G.pairs = function(iterable)
		local mt = getmetatable(iterable) or {}
		if mt.__pairs then
			return mt.__pairs(iterable)
		else
			return old_pairs(iterable)
		end
	end

	local old_ipairs = ipairs
	_G.ipairs = function(iterable)
		local mt = getmetatable(iterable) or {}
		if mt.__ipairs then
			return mt.__ipairs(iterable)
		else
			return old_ipairs(iterable)
		end
	end
end
